package tests;

import ai.abstraction.LightRush;
import ai.abstraction.StrategicDecisionMaking;
import ai.abstraction.pathfinding.AStarPathFinding;
import ai.core.AI;
import ai.core.ContinuingAI;
import gui.PhysicalGameStateJFrame;
import gui.PhysicalGameStatePanel;
import rts.GameState;
import rts.PhysicalGameState;
import rts.PlayerAction;
import rts.units.UnitTypeTable;

public class PlayGameWithStrategicDecisionMakingAI {
    public static void main(String args[]) throws Exception {
        UnitTypeTable utt = new UnitTypeTable();
        PhysicalGameState pgs = PhysicalGameState.load("maps/16x16/basesWorkers16x16.xml", utt);

        GameState gs = new GameState(pgs, utt);
        int MAXCYCLES = 10000;
        int PERIOD = 100;
        boolean gameover = false;

        PhysicalGameStatePanel pgsp = new PhysicalGameStatePanel(gs);
        PhysicalGameStateJFrame w = new PhysicalGameStateJFrame("Game State Visualizer", 640, 640, pgsp);

        AI ai1 = new StrategicDecisionMaking(utt, new AStarPathFinding());
        AI ai2 = new LightRush(utt, new AStarPathFinding());

        long nextTimeToUpdate = System.currentTimeMillis() + PERIOD;

        do {
            if (System.currentTimeMillis() >= nextTimeToUpdate) {
                PlayerAction pa1 = ai1.getAction(0, gs);
                PlayerAction pa2 = ai2.getAction(1, gs);

                gs.issueSafe(pa1);
                gs.issueSafe(pa2);

                gameover = gs.cycle();
                w.repaint();
                nextTimeToUpdate += PERIOD;
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } while (!gameover && gs.getTime() < MAXCYCLES);

        ai1.gameOver(gs.winner());
        ai2.gameOver(gs.winner());

        System.out.println("Game Over");
    }
}
