package ai.abstraction;

import java.util.ArrayList;

public class Rule {
    public ArrayList<Term> m_pattern;
    public Term m_effect;

    public Rule(Term effect) {
        m_pattern = new ArrayList<Term>();
        m_effect = effect;
    }

    /**
     * Add a Term to the Rule Pattern
     * @param term
     */
    public void addTerm(Term term) {
        m_pattern.add(term);
    }

    public void print() {
        System.out.println("Rule:");
        m_effect.print();
        for (Term term : m_pattern) {
            term.print();
        }
    }
}
