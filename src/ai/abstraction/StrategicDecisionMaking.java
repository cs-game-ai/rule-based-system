package ai.abstraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ai.abstraction.pathfinding.PathFinding;
import ai.core.AI;
import ai.core.ParameterSpecification;
import rts.GameState;
import rts.PhysicalGameState;
import rts.Player;
import rts.PlayerAction;
import rts.units.Unit;
import rts.units.UnitType;
import rts.units.UnitTypeTable;

public class StrategicDecisionMaking extends AbstractionLayerAI {

    public UnitTypeTable m_utt;
    public KnowledgeBase m_kb;
    public ArrayList<Rule> m_rules;

    private UnitType workerType;
    private UnitType baseType;
    private UnitType barracksType;
    private UnitType lightType;

    public StrategicDecisionMaking(UnitTypeTable utt, PathFinding a_pf) {
        super(a_pf);

        /** Init Base KB */
        m_kb = new KnowledgeBase();

        /** Init Types */
        m_utt = utt;
        workerType = utt.getUnitType("Worker");
        baseType = utt.getUnitType("Base");
        barracksType = utt.getUnitType("Barracks");
        lightType = utt.getUnitType("Light");

        /** Define Core Rules from Text File */
        m_rules = new ArrayList<Rule>();
        getRulesFromFile("src/ai/abstraction/rules.txt");
    }

    /** Fills the Knowledge Base with Relevant Information from the Game State */
    public void perception(int player, PhysicalGameState pgs) {
        /** Handle Resources */
        if (pgs.getPlayer(player).getResources() >= workerType.cost) {
            m_kb.addTerm(new Term("enoughResourcesFor", "Worker"));
        }
        if (pgs.getPlayer(player).getResources() >= baseType.cost) {
            m_kb.addTerm(new Term("enoughResourcesFor", "Base"));
        }
        if (pgs.getPlayer(player).getResources() >= barracksType.cost) {
            m_kb.addTerm(new Term("enoughResourcesFor", "Barracks"));
        }
        if (pgs.getPlayer(player).getResources() >= lightType.cost) {
            m_kb.addTerm(new Term("enoughResourcesFor", "Light"));
        }

        for (Unit u : pgs.getUnitsByPlayer(player)) {
            /** Handle Worker Logic */
            if (u.getType() == m_utt.getUnitType("Worker")) {
                m_kb.addTerm(new Term("own", "Worker", true));
            }

            /** Handle Base Logic */
            if (u.getType() == m_utt.getUnitType("Base")) {
                m_kb.addTerm(new Term("own", "Base", true));
            }

            /** Handle Barracks Logic */
            if (u.getType() == m_utt.getUnitType("Barracks")) {
                m_kb.addTerm(new Term("own", "Barracks", true));
            }
        }

        /** Explicitly Handle Negations */
        if (!m_kb.contains("own", "Worker")) {
            m_kb.addTerm(new Term("own", "Worker", false));
        }

        if (!m_kb.contains("own", "Base")) {
            m_kb.addTerm(new Term("own", "Base", false));
        }

        if (!m_kb.contains("own", "Barracks")) {
            m_kb.addTerm(new Term("own", "Barracks", false));
        }
    }

    /** Helper function to pull out values in parenthesis */
    private String[] decomposePredicate(String predicate) {
        return predicate
                .replaceAll("\"", "")
                .split("[\\(\\)]");
    }

    /** Parses rules from the given rules file */
    private void getRulesFromFile(String filename) {
        try {
            File rules = new File(filename);
            Scanner reader = new Scanner(rules);
            while (reader.hasNextLine()) {
                String line = reader.nextLine();
                if (line.length() == 0)
                    continue;
                if (line.charAt(0) == '#')
                    continue;
                /** Strip all unnecessary characters from the line */
                line = line
                        .replaceAll(" ", "")
                        .replaceAll("\\.", "");

                String[] terms = line.split(":-");
                String[] action = decomposePredicate(terms[0]);

                Rule rule = new Rule(new Term(action[0], action[1]));

                /** Generate Terms for the Rule Patterns */
                for (String predicate : terms[1].split(",")) {
                    if (predicate.equals("idle"))
                        continue;
                    String[] term = decomposePredicate(predicate);
                    String functor = term[0];
                    String type = term[1];
                    boolean value = functor.charAt(0) != '~';
                    if (value) {
                        rule.addTerm(new Term(functor, type, value));
                    } else {
                        rule.addTerm(new Term(functor.substring(1), type, value));
                    }
                }
                m_rules.add(rule);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not find the file...");
        }
    }

    /** Checks if a Rule's Pattern matches the information in the Knowledge Base */
    public boolean matchPattern(ArrayList<Term> terms) {
        boolean match = true;
        for (Term term : terms) {
            if (!m_kb.contains(term)) {
                match = false;
            }
        }
        return match;
    }

    /** Get the distance between two units */
    public int distance(Unit u1, Unit u2) {
        return Math.abs(u1.getX() - u2.getX()) + Math.abs(u1.getY() - u2.getY());
    }

    @Override
    public PlayerAction getAction(int player, GameState gs) throws Exception {

        PhysicalGameState pgs = gs.getPhysicalGameState();
        Player p = gs.getPlayer(player);
                             
        /** Load the Knowledge Base at the beginning of each Action Cycle */
        perception(player, pgs);

        ArrayList<Integer> reservedPositions = new ArrayList<Integer>();

        for (Unit unit : pgs.getUnitsByPlayer(player)) {
            ArrayList<Rule> possibleActions = new ArrayList<Rule>();
            for (Rule rule : m_rules) {
                /** If the rule does not apply to the current unit, move to the next unit */
                if (!rule.m_effect.m_type.equals(unit.getType().name)) continue;
                if (matchPattern(rule.m_pattern)) {
                    possibleActions.add(rule);
                }
            }

            /** If no actions should occur for the unit, move to the next unit */
            if (possibleActions.size() == 0) continue;

            /** Capture the first Rule and then apply the according actor based on the functor */
            Rule action = possibleActions.get(0);
            switch(action.m_effect.m_functor) {
                case "doTrainWorker":
                    this.handleTrainWorker(unit);
                    break;
                case "doBuildBase":
                    this.handleBuildBase(unit, reservedPositions, p, pgs);
                    break;
                case "doBuildBarracks":
                    this.handleBuildBarracks(unit, reservedPositions, p, pgs);
                    break;
                case "doTrainLight":
                    this.handleTrainLight(unit);
                    break;
                case "doHarvest":
                    this.handleHarvest(unit, player, pgs);
                    break;
                case "doAttack":
                    this.handleAttack(unit, p, pgs);
                    break;
            }
        }

        /** Clear the Knowledge Base at the end of each Action Cycle */
        m_kb.clear();

        return translateActions(player, gs);
    }

    /**
     * Handle Training Workers
     * @param unit
     */
    public void handleTrainWorker(Unit unit) {
        train(unit, workerType);
    }

    /**
     * Handle Building Bases
     * @param unit
     * @param reservedPositions
     * @param p
     * @param pgs
     */
    public void handleBuildBase(Unit unit, ArrayList<Integer> reservedPositions, Player p, PhysicalGameState pgs) {
        buildIfNotAlreadyBuilding(unit, baseType, unit.getX(), unit.getY(), reservedPositions, p, pgs);
    }

    /**
     * Handle Building Barracks
     * @param unit
     * @param reservedPositions
     * @param p
     * @param pgs
     */
    public void handleBuildBarracks(Unit unit, ArrayList<Integer> reservedPositions, Player p, PhysicalGameState pgs) {
        buildIfNotAlreadyBuilding(unit, barracksType, unit.getX(), unit.getY(), reservedPositions, p, pgs);
    }

    /**
     * Handle Training Light Soldiers
     * @param unit
     */
    public void handleTrainLight(Unit unit) {
        train(unit, lightType);
    }

    /**
     * Handle Harvesting (For Workers)
     * @param unit
     * @param player
     * @param pgs
     */
    public void handleHarvest(Unit unit, int player, PhysicalGameState pgs) {
        Unit closestBase = null;
        Unit closestResource = null;
        int resourceDist = 0;
        int baseDist = 0;
        // Find Closest Resource
        for (Unit resource : pgs.getUnits()) {
            if (resource.getType().isResource) {
                int dist = this.distance(resource, unit);
                if (closestResource == null || dist < resourceDist) {
                    closestResource = resource;
                    resourceDist = dist;
                }
            }
        }

        // Find Closest Base
        for (Unit base : pgs.getUnitsByPlayer(player)) {
            if (base.getType().isStockpile) {
                int dist = this.distance(base, unit);
                if (closestBase == null || dist < baseDist) {
                    closestBase = base;
                    baseDist = dist;
                }
            }
        }
        harvest(unit, closestResource, closestBase);
    }

    /**
     * Handle Attacking Enemy Units
     * @param unit
     * @param p
     * @param pgs
     */
    public void handleAttack(Unit unit, Player p, PhysicalGameState pgs) {
        Unit closestEnemy = null;
        int enemyDist = 0;
        for (Unit enemy : pgs.getUnits()) {
            if (enemy.getPlayer() >= 0 && enemy.getPlayer() != p.getID()) {
                int dist = this.distance(enemy, unit);
                if (closestEnemy == null || dist < enemyDist) {
                    closestEnemy = enemy;
                    enemyDist = dist;
                }
            }
        }
        attack(unit, closestEnemy);
    }

    @Override
    public AI clone() {
        /** Not Required for P4 */
        return null;
    }

    @Override
    public List<ParameterSpecification> getParameters() {
        /** Not Required for P4 */
        return null;
    }

}
