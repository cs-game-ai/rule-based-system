package ai.abstraction;

import java.util.ArrayList;

public class KnowledgeBase {
    ArrayList<Term> m_facts;

    public KnowledgeBase() {
        m_facts = new ArrayList<Term>();
    }

    /**
     * Add a Term to the Knowledge Base
     * @param term
     */
    public void addTerm(Term term) {
        m_facts.add(term);
    }

    /**
     * Check if a Term is contained in the Knowledge Base
     * @param term
     * @return
     */
    public boolean contains(Term term) {
        for (Term fact : m_facts) {
            if (fact.m_functor.equals(term.m_functor)
                && fact.m_type.equals(term.m_type) 
                && fact.m_value == term.m_value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a Term is contained in the Knowledge Base according to 
     * its functor and type
     * @param functor
     * @param type
     * @return
     */
    public boolean contains(String functor, String type) {
        for (Term fact : m_facts) {
            if (fact.m_functor == functor && fact.m_type == type) {
                return true;
            }
        }
        return false;
    }

    /**
     * Empty the Knowledge Base
     */
    public void clear() {
        m_facts.clear();
    }

    public void print() {
        for (Term fact : m_facts) {
            fact.print();
        }
    }
}
