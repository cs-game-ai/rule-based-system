package ai.abstraction;

public class Term {
    String m_functor;
    String m_type;
    int m_pid;
    int m_amount;

    /**
     * Handles Terms w/ Negations
     * TRUE => Positive (+)
     * FALSE => Negative (-)
     */
    boolean m_value;

    public Term(String functor, String type) {
        this(functor, type, 0);
    }

    public Term(String functor, String type, boolean value) {
        this(functor, type);
        m_value = value;
    }

    public Term(String functor, String type, int pid) {
        this(functor, type, pid, 0, true);
    }

    public Term(String functor, String type, int pid, int amount) {
        this(functor, type, pid, amount, true);
    }

    public Term(String functor, String type, int pid, int amount, boolean value) {
        m_functor = functor;
        m_type = type;
        m_pid = pid;
        m_amount = amount;
        m_value = value;
    }

    public boolean equals(Term term) {
        return m_functor == term.m_functor;
    }

    public void print() {
        System.out.printf("Term => Functor: %s; Type: %s; PID: %d; Amount: %d; Value %b\n",
                m_functor,
                m_type,
                m_pid,
                m_amount,
                m_value);
    }
}